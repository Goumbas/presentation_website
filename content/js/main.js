/*
*=================================
* Hugo UILite Portfolio v0.8
*=================================
*
* Free version https://uicard.io/products/hugo-uilite
* Pro version https://uicard.io/products/hugo-uilite-pro
* Demo https://demo.uicard.io/hugo-uilite-portfolio-demo/
*
* Coded By UICardio
*
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*
*/

const getAge = (dateOfBirth) => {
  const today = new Date();
  const birthday = new Date(dateOfBirth);

  let age = today.getFullYear() - birthday.getFullYear();
  const month = today.getMonth() - birthday.getMonth();
  const day = today.getDate() - birthday.getDate();

  if(month < 0 || (month === 0 && day < 0)) age --;

  return age;
};

$(document).ready(function(){
  const elements = $(".sidebar > .main-info *");

  for(let i = 0; i < elements.length; i++){
    setTimeout(function(){
      $(elements[i].tagName).addClass("bs");
    }, (400 * i) - 90 * i );
  }

  setTimeout(function(){
    $(".main-content").addClass("active");
  }, 1900);

  $('#my-age').html(getAge('7/1/1995'));

});